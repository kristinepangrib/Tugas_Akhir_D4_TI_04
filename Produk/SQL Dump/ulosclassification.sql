-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2018 at 09:12 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ulosclassification`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `ulos_category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `ulos_category`) VALUES
(1, 'Ulos Mangiring'),
(2, 'Ulos Ragi Hidup'),
(3, 'Ulos Sitolutuho'),
(4, 'Ulos Harungguan'),
(5, 'Ulos Ragi Hotang'),
(6, 'Ulos Sibolang'),
(7, 'Ulos Bintang Maratur'),
(8, 'Ulos Sadum');

-- --------------------------------------------------------

--
-- Table structure for table `ulos`
--

CREATE TABLE `ulos` (
  `id` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `image_path` varchar(200) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ulos`
--

INSERT INTO `ulos` (`id`, `image_name`, `image_path`, `id_category`) VALUES
(1, 'mangiring.png', 'storage/public/img_src/', 1),
(2, 'ragiidup.png', 'storage/public/img_src/', 2),
(3, 'sitoluntuho.png', 'storage/public/img_src/', 3),
(4, 'harungguan.png', 'storage/public/img_src/', 4),
(5, 'ragihotang.png', 'storage/public/img_src/', 5),
(6, 'sibolang.png', 'storage/public/img_src/', 6),
(7, 'bintang_maratur.png', 'storage/public/img_src/', 7),
(8, 'sadum.png', 'storage/public/img_src/', 8),
(83, 'Sadum2.png8KP.jpg', 'storage/public/img_src/', 8),
(84, 'Sibolang.jpgTIm.jpg', 'storage/public/img_src/', 6),
(85, 'test-ulos-sadum-benar.JPG6ZY.jpg', 'storage/public/img_src/', 2),
(86, 'test-sadum.pngtqi.jpg', 'storage/public/img_src/', 7),
(87, 'test-mangiring (2).pngnhj.jpg', 'storage/public/img_src/', 5),
(88, 'Sibolang.jpg0TF.jpg', 'storage/public/img_src/', 6),
(89, 'Sibolang.jpgwNR.jpg', 'storage/public/img_src/', 6),
(90, 'Sadum2.pngCff.jpg', 'storage/public/img_src/', 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ulos`
--
ALTER TABLE `ulos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `FK_ulos` (`id_category`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ulos`
--
ALTER TABLE `ulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ulos`
--
ALTER TABLE `ulos`
  ADD CONSTRAINT `FK_ulos` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
