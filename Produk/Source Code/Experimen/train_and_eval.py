# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 11:24:25 2018

@author: User
"""

import os,cv2
import numpy as np
import matplotlib.pyplot as plt

from sklearn.utils import shuffle
from sklearn.cross_validation import train_test_split

from keras import backend as K
K.set_image_dim_ordering('th')

from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD,RMSprop,Adam
from sklearn.model_selection import StratifiedKFold


#-----------------------------------------------------------------------------------------

PATH = os.getcwd()
# Define data path
data_path = '/dataset-ulos/Resize Image 1024'
data_dir_list = os.listdir(data_path)

img_rows=900
img_cols=900
num_channel=1
num_epoch=30

# Define the number of classes
num_classes = 8

#num_classes = 8

img_data_list=[]

for dataset in data_dir_list:
	img_list=os.listdir(data_path+'/'+ dataset)
	print ('Loaded the images of dataset-'+'{}\n'.format(dataset))
	for img in img_list:
		input_img=cv2.imread(data_path + '/'+ dataset + '/'+ img )
		input_img=cv2.cvtColor(input_img, cv2.COLOR_BGR2GRAY)
		input_img_resize=cv2.resize(input_img,(img_rows,img_cols))
		img_data_list.append(input_img_resize)

img_data = np.array(img_data_list)
img_data = img_data.astype('float32')
img_data /= 255
print (img_data.shape)

if num_channel==1:
	if K.image_dim_ordering()=='th':
		img_data= np.expand_dims(img_data, axis=1) 
		print (img_data.shape)
	else:
		img_data= np.expand_dims(img_data, axis=4) 
		print (img_data.shape)
		
else:
	if K.image_dim_ordering()=='th':
		img_data=np.rollaxis(img_data,3,1)
		print (img_data.shape)
		
#-----------------------------------------------------------------------------------------
# Assigning Labels

num_of_samples = img_data.shape[0]
labels = np.ones((num_of_samples,),dtype='int64')

# =============================================================================
# names = ['Ulos Sibolang', 'Ulos Sadum', 'Ulos Ragi Hidup','Ulos Mangiring', 'Ulos Harungguan', 'Ulos Ragi Hotang', 'Ulos Bintang Maratur', 'Ulos Sitolutuho']
# labels[0:632]=0
# labels[632:1440]=1
# labels[1440:2248]=2
# labels[2248:2912]=3
# labels[2912:3176]=4
# labels[3176:4272]=5
# labels[4272:4888]=6
# labels[4888:]=7
# # convert class labels to on-hot encoding
# =============================================================================

names = ['Ulos Bintang Maratur', 'Ulos Ragi Hidup', 'Ulos Sitolutuho','Ulos Sibolang', 'Ulos Ragi Hotang', 'Ulos Sadum', 'Ulos Harungguan', 'Ulos Mangiring']
labels[0:616]=0
labels[616:1424]=1
labels[1424:1536]=2
labels[1536:2168]=3
labels[2168:3264]=4
labels[3264:4072]=5
labels[4072:4336]=6
labels[4336:]=7

Y = np_utils.to_categorical(labels, num_classes)

#Shuffle the dataset
x,y = shuffle(img_data,Y, random_state=2)


seed = 7
np.random.seed(seed)
kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=seed)


input_shape=img_data[0].shape
counter = 0

for train_index, test_index in kfold.split(x,y[:,0]):
     print("TRAIN:", train_index, "\nTEST:", test_index)
     X_train, X_test = x[train_index], x[test_index]
     y_train, y_test = y[train_index], y[test_index]
     counter+=1
     nilai_learning_rate=0.001
     opt=Adam(lr=nilai_learning_rate)
     np.savetxt('/output/'+str(counter)+'-train_index.csv', train_index, fmt='%d', delimiter=',')
     np.savetxt('/output/'+str(counter)+'-test_index.csv', test_index, fmt='%d', delimiter=',')
     
     model = Sequential()
             
     model.add(Convolution2D(32, 3, strides=(3, 3), border_mode='same',input_shape=input_shape))
                 
     model.add(Activation('relu'))
                 
     model.add(Convolution2D(64, 3, strides=(3, 3)))
                 
     model.add(Activation('relu'))
                 
     model.add(MaxPooling2D(pool_size=(2, 2)))
                 
     model.add(Dropout(0.5))
                 
     model.add(Convolution2D(128, 3, strides=(3, 3)))
                 
     model.add(Activation('relu'))
                 
     model.add(Convolution2D(256, 3, strides=(3, 3)))
                 
     model.add(Activation('relu'))
                 
     model.add(MaxPooling2D(pool_size=(2, 2)))
                 
     model.add(Dropout(0.5))
                 
     model.add(Flatten())
                 
     model.add(Dense(128))
                 
     model.add(Activation('relu'))
    
     model.add(Dense(128))
                 
     model.add(Activation('relu'))
    
     model.add(Dense(128))
                 
     model.add(Activation('relu'))
                 
     model.add(Dense(num_classes))
                 
     model.add(Activation('softmax'))
     model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=["accuracy"])    
     hist = model.fit(X_train, y_train, epochs=num_epoch, batch_size=16, validation_data=(X_test, y_test), verbose=1)
     score = model.evaluate(X_test, y_test, batch_size= 32, verbose=0)
     model.save('/output/'+str(counter)+'-K-fold-model.hdf5')
     print('K-Fold -', counter)    
     print('==================================')
     print('Test Loss:', score[0])
     print('Test accuracy:', score[1])
     print('==================================')
     X_train = []
     X_test = []
     y_train = []
     y_test = []
