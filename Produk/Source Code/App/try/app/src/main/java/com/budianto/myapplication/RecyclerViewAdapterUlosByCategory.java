package com.budianto.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.budianto.myapplication.models.UlosModel;
import com.budianto.myapplication.network.Config;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class RecyclerViewAdapterUlosByCategory extends RecyclerView.Adapter<RecyclerViewAdapterUlosByCategory.MyViewHolder> {

    private Context mContext ;
    private List<UlosModel> mData ;


    public RecyclerViewAdapterUlosByCategory(Context mContext, List<UlosModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_ulos_model,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_ulos_title.setText(mData.get(position).getImage_name());
        Glide glide = null;
        String imgUrl = Config.BASE_URL +mData.get(position).getImage_path()+mData.get(position).getImage_name();
        glide.with(holder.img_ulos_thumbnail.getContext())
                .load(imgUrl)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .override(256, 250)
                .centerCrop()
                .fitCenter()
                .crossFade()
                .into(holder.img_ulos_thumbnail);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_ulos_title;
        ImageView img_ulos_thumbnail;
        CardView cardView ;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_ulos_title = itemView.findViewById(R.id.ulos_title_id);
            img_ulos_thumbnail = itemView.findViewById(R.id.ulos_img_id);
            cardView = itemView.findViewById(R.id.cardviewulos_id);
        }
    }
}

