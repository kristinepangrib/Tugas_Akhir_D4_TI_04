package com.budianto.myapplication.network;

import com.budianto.myapplication.models.UlosModel;

public class BaseResponse {
    private boolean success;
    private String message;
    private UlosModel data;

    public UlosModel getData() {
        return data;
    }

    public void setData(UlosModel data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success){
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
