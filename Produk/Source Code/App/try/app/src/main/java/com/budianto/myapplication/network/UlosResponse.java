package com.budianto.myapplication.network;

import com.budianto.myapplication.Ulos;
import com.budianto.myapplication.models.UlosModel;

import java.util.List;

public class UlosResponse {
    private boolean success;
    private String message;
    private List<UlosModel> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success){
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UlosModel> getUlosList() {
        return data;
    }

    public void setUlosList(List<UlosModel> ulosList) {
        this.data = ulosList;
    }
}
