package com.budianto.myapplication.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UlosModel extends RealmObject {
    @PrimaryKey
    private int id;

    private String image_name;
    private String image_path;
    private int id_category;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }
}
