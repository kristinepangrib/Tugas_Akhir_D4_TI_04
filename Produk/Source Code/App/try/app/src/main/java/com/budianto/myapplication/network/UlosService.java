package com.budianto.myapplication.network;

import com.budianto.myapplication.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UlosService {
    private UlosInterface ulosInterface;

    public UlosService(){
        OkHttpClient.Builder okhttpBuilder = new OkHttpClient().newBuilder();
        okhttpBuilder.connectTimeout(200, TimeUnit.SECONDS);
        okhttpBuilder.writeTimeout(200, TimeUnit.SECONDS);
        okhttpBuilder.readTimeout(200, TimeUnit.SECONDS);
        okhttpBuilder.retryOnConnectionFailure(true);

        if(BuildConfig.DEBUG){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okhttpBuilder.addInterceptor(interceptor);
        }

        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .client(okhttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ulosInterface = retrofit.create(UlosInterface.class);
    }

    public void classify(MultipartBody.Part photo, Callback callback){
        ulosInterface.classify(photo).enqueue(callback);
    }

    public void getDataUlos(Callback callback){
        ulosInterface.getDataUlos().enqueue(callback);
    }

    public void getDataUlosByCategory(int idCategory, Callback callback){
        ulosInterface.getDataUlosByCategory(idCategory).enqueue(callback);
    }
}
