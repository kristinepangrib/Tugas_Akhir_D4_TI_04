package com.budianto.myapplication;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String TITLE = "Home";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private List<Ulos> lstUlos;
    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        lstUlos = new ArrayList<>();
        lstUlos.add(new Ulos("Ulos Mangiring", R.drawable.mangiring));
        lstUlos.add(new Ulos("Ulos Ragi Hidup", R.drawable.ragiidup));
        lstUlos.add(new Ulos("Ulos Sitolutuho", R.drawable.sitoluntuho));
        lstUlos.add(new Ulos("Ulos Harungguan", R.drawable.harungguan));
        lstUlos.add(new Ulos("Ulos Ragi Hotang", R.drawable.ragihotang));
        lstUlos.add(new Ulos("Ulos Sibolang", R.drawable.sibolang));
        lstUlos.add(new Ulos("Ulos Bintang Maratur", R.drawable.bintang_maratur));
        lstUlos.add(new Ulos("Ulos Sadum", R.drawable.sadum));

        RecyclerView myrv = v.findViewById(R.id.recyclerviewulos_id_1);
        RecyclerViewAdapterUlos myAdapter = new RecyclerViewAdapterUlos(getContext(), lstUlos);
        myrv.setLayoutManager(new GridLayoutManager(getContext(), 2));
        myrv.setAdapter(myAdapter);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
