package com.budianto.myapplication;

//import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budianto.myapplication.models.UlosModel;
import com.budianto.myapplication.network.BaseResponse;
import com.budianto.myapplication.network.UlosResponse;
import com.budianto.myapplication.network.UlosService;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailUlosActivity extends AppCompatActivity {
    List<UlosModel> listUlosByCategory;
    private UlosService ulosService;
    Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ulos);

        Intent mIntent = getIntent();
        int idCategory = mIntent.getIntExtra("idCategory", 0);
        String nama_ulos = mIntent.getStringExtra("nama_ulos").toString();

//        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(nama_ulos);
        realm = Realm.getDefaultInstance();
        RealmQuery<UlosModel> query = realm.where(UlosModel.class);
        query.equalTo("id_category", idCategory);

        RealmResults<UlosModel> result = query.findAll();

        result = result.sort("id", Sort.ASCENDING);

        List<UlosModel> copied = realm.copyFromRealm(result);

        listUlosByCategory = realm.where(UlosModel.class).equalTo("id", idCategory).findAll();
        System.out.println(listUlosByCategory.toArray());
        RecyclerView myrv = findViewById(R.id.recyclerviewulos_id);
        RecyclerViewAdapterUlosByCategory myAdapter = new RecyclerViewAdapterUlosByCategory(this, copied);
        myrv.setLayoutManager(new GridLayoutManager(this, 1));
        myrv.setAdapter(myAdapter);
    }

    public List<UlosModel> getUlosByCategory(int idCategory){
        ulosService = new UlosService();
        ulosService.getDataUlosByCategory(idCategory, new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                UlosResponse ulosResponse = (UlosResponse) response.body();
                if(ulosResponse != null) {
                    listUlosByCategory= ulosResponse.getUlosList();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });
        return listUlosByCategory;
    }
}
