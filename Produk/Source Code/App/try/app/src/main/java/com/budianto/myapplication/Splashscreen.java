package com.budianto.myapplication;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.budianto.myapplication.models.UlosModel;
import com.budianto.myapplication.network.UlosResponse;
import com.budianto.myapplication.network.UlosService;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Splashscreen extends Activity {
    private UlosService ulosService;
    private boolean isRunning;
    Realm realm;
    ImageView iv;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.isRunning = true;
        setContentView(R.layout.activity_splashscreen);
        realm = Realm.getDefaultInstance();
        fecthData();

        iv = findViewById(R.id.logo);

        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_right);
        iv.setAnimation(animation);
        startSplash();
    }

    private void startSplash() {
        new Thread(new C00511()).start();
    }

    private synchronized void doFinish() {
        if (this.isRunning) {
            this.isRunning = false;
            Intent i = new Intent(this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
    class C00511 implements Runnable {
        C00511() {
        }
        public void run() {
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                Splashscreen.this.runOnUiThread(new C00501());
            }
        }
        class C00501 implements Runnable {
            C00501() {
            }
            public void run() {
                Splashscreen.this.doFinish();
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.isRunning = false;
        finish();
        return true;
    }

    public void fecthData(){
        ulosService = new UlosService();
        ulosService.getDataUlos(new Callback<UlosResponse>() {
            @Override
            public void onResponse(Call<UlosResponse> call, Response<UlosResponse> response) {
                final UlosResponse ulosResponse = (UlosResponse) response.body();
                if(ulosResponse != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmList<UlosModel> _newsList = new RealmList<>();
                            _newsList.addAll(ulosResponse.getUlosList());
                            realm.insertOrUpdate(_newsList); // <-- insert unmanaged to Realm
                            System.out.println(ulosResponse.getUlosList().toArray());

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
