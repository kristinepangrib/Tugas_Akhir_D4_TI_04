package com.budianto.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;

import java.util.List;

public class RecyclerViewAdapterUlos extends RecyclerView.Adapter<RecyclerViewAdapterUlos.MyViewHolder> {

    private Context mContext ;
    private List<Ulos> mData ;


    public RecyclerViewAdapterUlos(Context mContext, List<Ulos> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_ulos,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_ulos_title.setText(mData.get(position).getTitleUlos());
        holder.img_ulos_thumbnail.setImageResource(mData.get(position).getThumbnailUlos());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(mContext, DetailUlosActivity.class);
                myIntent.putExtra("idCategory", position+1);
                myIntent.putExtra("nama_ulos", mData.get(position).getTitleUlos());
                mContext.startActivity(myIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_ulos_title;
        ImageView img_ulos_thumbnail;
        CardView cardView ;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_ulos_title = itemView.findViewById(R.id.ulos_title_id);
            img_ulos_thumbnail = itemView.findViewById(R.id.ulos_img_id);
            cardView = itemView.findViewById(R.id.cardviewulos_id);
        }
    }
}

