package com.budianto.myapplication.network;

import com.budianto.myapplication.Ulos;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UlosInterface {
    @Multipart
    @POST(Config.API_CLASSIFY)
    Call<BaseResponse> classify(@Part MultipartBody.Part photo);

    @GET(Config.API_GET_DATA)
    Call<UlosResponse> getDataUlos();

    @GET(Config.API_GET_DATA_BY_CATEGORY)
    Call<List<Ulos>> getDataUlosByCategory(@Path("id") int categoryId);

}
