package com.budianto.myapplication.network;

public class Config {
    public static final String BASE_URL = "http://192.168.43.252/api-tenun/"; //Your local IP Address
    public static final String API_GET_DATA = BASE_URL + "ulos";
    public static final String API_GET_DATA_BY_CATEGORY = BASE_URL + "ulosByCategory/{id}";
    public static final String API_CLASSIFY = BASE_URL + "classification";
}